// This is a Revision History, it is simply a table
// - No heading, just a table https://docs.asciidoctor.org/asciidoc/latest/sections/special-section-titles/
// - Does not appear in ToC
:table-caption!:

{nbsp}

.Revision History
[cols="15,85"]
[frame=ends]
|===
|*Current:* +
*Previous:*
|*Doc_State, Doc_IssueDate* +
*None*
|*Page*
|*Initial release*
|{nbsp}
|{nbsp}
|===

// This turns back on "Table" captions
:table-caption: Table

<<<