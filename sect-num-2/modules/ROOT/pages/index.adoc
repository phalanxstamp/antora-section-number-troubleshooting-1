ifdef::loader-assembler[]

:!sectnums:

include::front-legal-notice.adoc[]
include::front-revision-history.adoc[]
include::front-third-party-trademark.adoc[]
toc::[]

include::front-preface.adoc[]

:sectnums:

include::numbered-content-section-1.adoc[]
include::numbered-content-section-2.adoc[]

:!sectnums:
include::back-references.adoc[]
include::back-terminology.adoc[]

endif::[]