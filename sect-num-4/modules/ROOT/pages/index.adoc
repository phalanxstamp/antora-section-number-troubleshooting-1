ifdef::loader-assembler[]

:!sectnums:

include::front-legal-notice.adoc[]
include::front-revision-history.adoc[]
include::front-third-party-trademark.adoc[]
toc::[]

include::front-preface.adoc[]

:sectnums:
endif::[]

//This is where the HTML content goes
ifndef::loader-assembler[]

= HTML Home
v1.0
:toc:

This content should only appear in the HTML output.
This is a = level.

== Subsection
Here is some more content at a == level.

endif::[]